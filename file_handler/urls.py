from django.urls import path, include
from .views import *
urlpatterns = [
    path('all-file', fetch_all, name="fetch_all"),
    # path('file', DocumentUploadView.as_view(), name="doc_upload"),
    path('file-compress', upload, name="file_compressor"),
    # path('file-compress', FileUploadView.as_view(), name="file_compressor"),
    path('file/<int:pk>', FileViewSet.as_view({'get': 'retrieve'}), name="file_retrieve"),
    path('download/<str:filename>', download, name="download")
]
