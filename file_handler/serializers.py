from rest_framework import serializers
from .models import *
class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'

class FileSerializer(serializers.ModelSerializer):

    # def get_id(self, obj):
    #     obj
    class Meta:
        model =  File
        fields = '__all__'

