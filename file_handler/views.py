from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from rest_framework.parsers import FileUploadParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.encoding import smart_str
from rest_framework import status, viewsets
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.shortcuts import get_object_or_404
from .models import *
from django.conf import settings
from .serializers import *
import zipfile
from django.views.decorators.csrf import csrf_exempt
from functools import partial
import os, io, logging, pika, types, threading
# Create your views here.
class BackgroundProcess():
    # EXCHANGE_TYPE = "direct"
    # EXCHANGE_NAME = "1706039471"
    # ROUTING_KEY   = "babi"


    # ROUTING_KEY berisi UNIQ ID yang diterima dari Server 1
    # RABBITMQ_HOST = 152.118.148.95
    # RABBITMQ_PORT = 5672
    # RABBITMQ_WEBSOCKET_PORT = 15674
    def __init__(self, queue="hello"):
        self.credentials = pika.PlainCredentials('0806444524', '0806444524')
        self.parameters  = pika.ConnectionParameters(host="152.118.148.95", port=5672 ,credentials=self.credentials, virtual_host='/0806444524')
        self.connection = pika.BlockingConnection(parameters=self.parameters)

        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange="1706039471", exchange_type="direct")

   

    def publish(self, payload="", routing_key=""):
        self.channel.basic_publish(
            exchange='1706039471',
            routing_key=routing_key,
            body=str(payload)
        )


    
    def stop(self):
        self.stopping = True
        self.close_connection()


perc = [10,20,30,40,50,60,70,80,90,100]
global bp
bp = BackgroundProcess('upload')



def fetch_all(request):
    obj = Document.objects.all()
    data = serializers.serialize('python', Document.objects.all())
    print(data)
    return JsonResponse(data, safe=False)
    
@csrf_exempt
def upload(request):
    uniqueId = request.META['HTTP_UNIQUEID']
    global the_bytes
    global the_obytes
    global perc
    global bp

    the_bytes = 0
    the_obytes = 0
    perc = [10,20,30,40,50,60,70,80,90,100]
    # if request.method=='POST':
    files = request.FILES['files']
    file_name = files.name
    file_name_formatted = file_name.split('.')[0].replace(" ", "_").replace("-", "_")
    handle_upload(request.FILES['files'], file_name)

    threading.Thread(target=write_zip, args=[file_name_formatted, file_name, uniqueId]).start()





    return JsonResponse({"foo": "bar"})

def handle_upload(f, name):
    destination = open(os.path.join(os.path.relpath('media/'), f.name), 'wb+')
    for chunck in f.chunks():
        destination.write(chunck)
        print('masuk')
    destination.close()

def write_zip(file_name_formatted, file_name, uniqueId):
    global the_bytes
    global the_obytes
    global perc
    global bp

    the_bytes = 0
    the_obytes = 0
    zip_name = os.path.join(settings.MEDIA_ROOT,file_name_formatted + ".zip")
    print(uniqueId)
    with zipfile.ZipFile(zip_name, 'w', compression=zipfile.ZIP_DEFLATED) as zip:
        zip.fp.write = types.MethodType(partial(progress, os.path.getsize(os.path.join(settings.MEDIA_ROOT, file_name)), zip.fp.write, uniqueId,file_name_formatted), zip.fp)
        zip.write(os.path.join(settings.MEDIA_ROOT, file_name))

    bp.publish(payload="done http://localhost:8080/download/" + file_name_formatted, routing_key=uniqueId)

    return {"message":"Success!", "link":"/download/"+ file_name_formatted+".zip"}

def download(request, filename):
    print(os.path.join(settings.MEDIA_ROOT, filename))
    zip_file = open(os.path.join(settings.MEDIA_ROOT, filename), 'rb')

    response = HttpResponse(zip_file,content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(filename)
    # response['X-Sendfile'] = smart_str(os.path.join(settings.MEDIA_ROOT, str(filename)))
    print(smart_str(os.path.join(settings.MEDIA_ROOT, (filename))))
    
    return response




class FileUploadView(APIView):
    parser_class = (FileUploadParser, MultiPartParser)



    def get(self, request):
        all_files = File.objects.all()
        file_serializer = FileSerializer(all_files, many=True)
        files = File.objects.get(id=1)
        file_name = str(files.files).split('/')[1].split('.')[0]
        the_file = os.path.join(settings.MEDIA_ROOT, str(files.files))


        print(str(files.files).split('/'))

        return Response(file_serializer.data, status=status.HTTP_200_OK)


class FileViewSet(viewsets.ViewSet):

    def retrieve(self, request, pk=None):
        queryset = File.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = FileSerializer(user)
        return Response(serializer.data)

def progress(total_size, original_write,uniqueId, filename, self, buf):
    global the_bytes
    global the_obytes
    global bp
    the_bytes += len(buf)
    the_obytes += 1024 * 8  # Hardcoded in zipfile.write
    percent = int(100 * the_obytes / total_size)
    global perc
    if percent in perc:
        try:
            if len(perc) > 0:
                perc = perc[1:]
                bp.publish(payload=str("progress " + str(percent)), routing_key="uniqueId")
        except:
            bp = BackgroundProcess('upload')
            bp.publish(payload=str("progress " + str(percent)), routing_key='uniqueId')
    return original_write(buf)


