from django.db import models

# Create your models here.
class Document(models.Model):
    name = models.CharField(max_length=512, blank=False)
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
    

class File(models.Model):
    name = models.CharField(max_length=255)
    files = models.FileField(upload_to='media/')

    def __str__(self):
        return self.name